#include <iostream>
#include <Windows.h>
#include <string>
#include <ctime>
#include <iomanip>
using namespace std;

class Animal
{
private:
	

public:
	Animal() {}
	
	virtual void Voice()
	{
		std::cout << "Text";
	}

};

class Dog : public Animal
{
private:
	
public:
	Dog(){}

	void Voice() 
	{
		std::cout <<"\n\tWoof!";
	}	
};

class Cat :public Animal
{
private:

public:
	Cat(){}

	void Voice() override
	{
		std::cout << "\n\tMeow!";
	}
};
class Cow :public Animal
{
private:

public:
	Cow() {}

	void Voice() override
	{
		std::cout << "\n\tMooo!";
	}
};

int main()
{
	SetConsoleCP(1251);       //���� ��� �������� ������
	SetConsoleOutputCP(1251); //����� ��� �������� ������
	
	Animal* V[3];
	V[0] = new Dog;
	V[1] = new Cat;
	V[2] = new Cow;
	
    for (int j = 0; j < 3; j++) 
	{
		V[j]->Voice();
	}
	std::cout << "\n";

	
	return 0;
}